# GPGSigVerifier -*- mode: python; coding: utf-8 -*-

# A class for verifying signed files

# Copyright (c) 2002 Colin Walters <walters@gnu.org>

# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os, subprocess

class GPGSigVerifierException(Exception):
    def __init__(self, value):
        self._value = value
    def __str__(self):
        return repr(self._value)

class GPGSigVerificationFailure(Exception):
    def __init__(self, value, output):
        self._value = value
        self._output = output
    def __str__(self):
        return repr(self._value)

    def getOutput(self):
        return self._output

class GPGSigVerifier:
    def __init__(self, keyrings, gpgv=None):
        self._keyrings = keyrings
        if not gpgv:
            gpgv = '/usr/bin/gpgv'
        if not os.access(gpgv, os.X_OK):
            raise GPGSigVerifierException("Couldn't execute \"%s\"" % gpgv)
        self._gpgv = gpgv

    def verify(self, filename, sigfilename=None):
        cmdline = [self._gpgv]
        for keyring in self._keyrings:
            cmdline.extend(['--keyring', keyring])
        if sigfilename:
            cmdline.append(sigfilename)
        cmdline.append(filename)
        try:
            return subprocess.check_output(cmdline).decode('utf-8')
        except subprocess.CalledProcessError as e:
            if e.returncode > 0:
                msg = "gpgv exited with error code %d" % e.returncode
            else:
                msg = "gpgv died with signal %d" % -e.returncode
            raise GPGSigVerificationFailure(msg, e.output)

# vim:ts=4:sw=4:et:
